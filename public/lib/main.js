// Globals
var API = 'https://esi.tech.ccp.is/latest';
var ZKILL = 'https://zkillboard.com/api';
var displayedchars = {};
var displayImages = false;

// Credit to Remy Sharp
// http://remysharp.com/2010/07/21/throttling-function-calls/
function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
            f.apply(context, args);
        },
        delay || 800);
    };
}

function makeLink(href, txt) {
    var e = document.createElement('a');
    e.textContent = txt;
    e.setAttribute('href', href);
    e.setAttribute('target', '_blank');
    return e;
}

function shareLink() {
    var link = window.location.href.split('#')[0] + '#chars=' +
        LZString.compressToEncodedURIComponent(
            document.getElementById('chars').value
        );
    $('#share').html(
        makeLink(link, link)
    );
    $('#share').fadeIn();
}

function toggleImg() {
    if(displayImages) {
        displayImages = false;
        $('#dimg').text('Show Images');
    } else {
        displayImages = true;
        $('#dimg').text('Hide Images');
    }
}

function mkCorp(data, p, v, name) {
    var corp;
    if(
        data.url == '' ||
        data.url == 'http://' ||
        data.url == 'None'
    ) {
        corp = document.createElement('span');
    } else {
        corp = document.createElement('a');
        corp.setAttribute('href', data.url);
    }

    corp.setAttribute('class', 'corp');
    corp.textContent =
        data.name +
        ' [' + data.ticker + '] (' +
        data.member_count + ')';
    p.appendChild(corp);

    // Is CEO?
    if (data.ceo_id == v.character_id) {
        name.textContent += ' (CEO)';
    }
}

function mkAlliance(data, p) {
    var corp = document.createElement('span');
    corp.setAttribute('class', 'alliance');
    corp.textContent =
        data.name +
        ' [' + data.ticker + ']';
    p.appendChild(corp);
}

$(document).ready(function() {
    $('#chars').submit(function(e) {
        e.preventDefault();

        // Split the things
        var characters = $('#chars').val().split("\n");
        var charIDs = new Array();
        var cmap = {};
        console.log(characters);

        // Initial loop to check for existing characters
        var copy = characters.slice();
        $.each(copy, function(i,v){
            console.log("Checking " + v);
            if(v in displayedchars) {
                console.log("Already have " + v);
                // Must recalculate index each splice
                // very inefficent
                // TODO: move entirely to associative arrays
                characters.splice(characters.indexOf(v), 1)
            }
        });
        console.log(characters);

        if (
            characters.length < 1 ||
            characters[0] == '' ||
            characters[0].length < 3
        ) { return; }

        // Add a new break
        $('#results').append(document.createElement('hr'));

        // Create an array of deferred get requests
        var gets = new Array();
        $.each(characters, function(i,v){
            // get charID
            console.log("building request for "+v);
            if(v.length < 3) { return; }
            var d;
            if(sessionStorage.getItem(v)) {
                var id = sessionStorage.getItem(v);
                cmap[id] = v;
                charIDs.push(id);
                displayedchars[v] = true;
                d = $.Deferred();
                d.resolve();
                console.log(v + " is cached as " + id);
            } else {
                d = $.get(API + "/search/",
                { categories: 'character'
                , datasource: 'tranquility'
                , language: 'en-us'
                , search: v
                , strict: true
                }
                , function(data){
                    var c = data.character;
                    if(!c) {
                        console.log("Fake character in the stack?");
                        return false; //break
                    }

                    if(c.length > 1) {
                        console.log("More than 1 match??");
                    } else {
                        var id = parseInt(c[0], 10);
                        console.log("Pushing " + id);
                        charIDs.push(id);
                        cmap[id] = v;
                        displayedchars[v] = true;

                        // Add to cache
                        sessionStorage.setItem(v, id);
                    }
                });
            }

            gets.push(d);
            console.log(gets.length + " reqs in deferred array");
        });

        // Send bulk request for all character infos
        // as a promise on the deferred get request array
        $.when.apply($,gets).then(function() {
            // Sanity check
            if(charIDs.length < 1) {
                console.log("No chars to pull");
                return false;
            }
            console.log("sending bulk request for " + charIDs);
            $.ajax({
                type: 'POST',
                url: API + '/characters/affiliation/',
                crossDomain: true,
                data: JSON.stringify(charIDs),
                dataType: 'json',
                contentType: 'application/json',
                success: function(data) {
                    console.log("Returned " + data.length);
                    // populate results container
                    $.each(data, function(i,v){
                        var p = document.createElement('div');
                        p.setAttribute('class', 'flex');
                        var ibox = document.createElement('div');
                        ibox.setAttribute('class', 'fleximg');
                        var name = document.createElement('span');
                        name.setAttribute('class', 'cname');

                        // link bar
                        var links = document.createElement('div');
                        links.setAttribute('class', 'linkbar');

                        // zkillboard
                        links.appendChild(
                            makeLink(
                                'https://zkillboard.com/character/' +
                                v.character_id + '/',
                                'zKill'
                            )
                        );

                        // Eve Who
                        links.appendChild(
                            makeLink(
                                'https://evewho.com/pilot/' +
                                cmap[v.character_id].replace(/ /g, '+'),
                                'EveWho'
                            )
                        );

                        //eve overmind
                        links.appendChild(
                            makeLink(
                                'https://www.eveovermind.com/' +
                                'searchResults.php?type=P&ID=' +
                                v.character_id,
                                'OverMind'
                            )
                        );

                        // Add character name
                        name.textContent = cmap[v.character_id];

                        // Get Corp Info
                        if(sessionStorage.getItem(v.corporation_id)) {
                            console.log('Pulling corporation from cache');
                            mkCorp(
                                JSON.parse(
                                    LZString.decompress(
                                        sessionStorage.getItem(
                                            v.corporation_id
                                        )
                                    )
                                ), p, v, name
                            );
                        } else {
                            $.get(API + '/corporations/'
                                + v.corporation_id + '/'
                                , function(data) {
                                    sessionStorage.setItem(
                                        v.corporation_id,
                                        LZString.compress(JSON.stringify(data))
                                    );
                                    mkCorp(data, p, v, name);
                                }
                            );
                        }

                        // Get Alliance Info
                        if('alliance_id' in v) {
                            if(sessionStorage.getItem(v.alliance_id)) {
                                console.log('Pulling alliance from cache');
                                mkAlliance(
                                    JSON.parse(
                                        LZString.decompress(
                                            sessionStorage.getItem(
                                                v.alliance_id
                                            )
                                        )
                                    ), p
                                );
                            } else {
                                $.get(API + '/alliances/'
                                    + v.alliance_id + '/'
                                    , function(data) {
                                        sessionStorage.setItem(
                                            v.alliance_id,
                                            LZString.compress(
                                                JSON.stringify(data)
                                            )
                                        );
                                        mkAlliance(data, p);
                                    }
                                );
                            }
                        }

                        // Get zkill stats
                        $.get(ZKILL + '/stats/characterID/'
                            + v.character_id + '/'
                            , function(data) {
                                var k = (data.shipsDestroyed ?
                                    data.shipsDestroyed : '0');
                                var d = (data.shipsLost ?
                                    data.shipsLost : '0');
                                var solo = (data.soloKills ?
                                    data.soloKills : '0');
                                var li = document.createElement('span');
                                li.setAttribute('class', 'zkillstats');
                                li.textContent = 'K/D: '
                                    + k + '/' + d
                                    + ' Solo: ' + solo
                                    + ' SecStat: '
                                    + data.info.secStatus.toFixed(2);
                                p.appendChild(li);

                                // Check 'threat' lvl
                                if(solo > data.soloLosses && k > d) {
                                    p.className += ' redbox';
                                } else if(k > d) {
                                    p.className += ' orangebox';
                                }

                                if ((!k && !d) || (k == 0 && d == 0)) {
                                    return;
                                }

                                // Get zkill W-Space KMs
                                $.get(ZKILL + '/w-space/kills/characterID/'
                                    + v.character_id + '/'
                                    , function(data) {
                                        var kills = data.length;
                                        var avg = (
                                            data.reduce( function(x, y) {
                                                return x + y.attackers.length
                                            }, 0
                                        ) / kills);
                                        avg = (avg) ? avg.toFixed(0) : '0';

                                        /** Credit to:
                                         * https://stackoverflow.com/questions/17313268/efficiently-find-the-number-of-occurrences-a-given-value-has-in-an-array
                                         **/
                                        var systems = data.map(
                                            function(o) { return o.solar_system_id }
                                        ).reduce(
                                            function(n, o) {
                                                n[o] =
                                                    n.hasOwnProperty(o) ? n[o] + 1 : 1;
                                                return n;
                                            },{}
                                        );
                                        var topSys = Object.keys(systems).sort(
                                            function(a,b) {
                                                return systems[b]-systems[a];
                                            }
                                        ).shift();

                                        // Resolve system ID
                                        var l2 = document.createElement('span');
                                        l2.setAttribute('class', 'topsys');
                                        if(sessionStorage.getItem(topSys)) {
                                            console.log('Pulling top system from cache');
                                            topSys = LZString.decompress(
                                                sessionStorage.getItem(topSys)
                                            );
                                            l2.textContent = 'Top System: ' + topSys;
                                            p.appendChild(l2);
                                        } else if(topSys) {
                                            $.get(API + '/universe/systems/' +
                                                topSys + '/'
                                                , function(data) {
                                                    sessionStorage.setItem(
                                                        topSys,
                                                        LZString.compress(
                                                            data.name
                                                        )
                                                    );
                                                    topSys = data.name;
                                                    l2.textContent = 'Top System: ' + topSys;
                                                    p.appendChild(l2);
                                                }
                                            );
                                        }

                                        var li = document.createElement('span');
                                        li.setAttribute('class', 'zkillkm');
                                        li.textContent = 'WH Kills: ' +
                                            kills + ' AVG FLT: ' + avg;
                                        p.appendChild(li);
                                    }
                                );

                            });

                        p.appendChild(name);
                        p.appendChild(ibox);
                        p.appendChild(links);
                        $('#results').append(p);

                        if (!displayImages)
                            return;

                        /* Don't directly cache these, just assume the
                         * server/client caching will handle it properly */

                        // Get character portrait
                        $.get(API + '/characters/'
                            + v.character_id + '/portrait/'
                            , function(data) {
                                var li = document.createElement('span');
                                li.setAttribute('class', 'chimg');
                                var img = document.createElement('img');
                                img.src = data.px128x128.
                                    replace(/http:\/\//,'https://');
                                li.appendChild(img);
                                ibox.appendChild(li);
                        });

                        // Get Corp Logo
                        $.get(API + '/corporations/'
                            + v.corporation_id + '/icons/'
                            , function(data) {
                                var li = document.createElement('span');
                                li.setAttribute('class', 'corpimg');
                                var img = document.createElement('img');
                                img.src = data.px128x128.
                                    replace(/http:\/\//, 'https://');
                                li.appendChild(img)
                                ibox.appendChild(li);
                        });

                        // Get Alliance Logo
                        if('alliance_id' in v) {
                            $.get(API + '/alliances/'
                                + v.alliance_id + '/icons/'
                                , function(data) {
                                    var li = document.createElement('span');
                                    li.setAttribute('class', 'allyimg');
                                    var img = document.createElement('img');
                                    img.src = data.px128x128.
                                        replace(/http:\/\//, 'https://');
                                    li.appendChild(img);
                                    ibox.appendChild(li);
                            });
                        }
                    });
                },
                error: function() {
                    console.log("POST failed!");
                }
            });
        });
    });

    // Bind submission stuff
    //$('#chars').on('paste',function(){ $('#chars').submit() });
    $('#chars').keyup(throttle(function(){
        $('#chars').submit();
    }));

    // UI Buttons
    $('#cres').click(function(){$('#results').empty();displayedchars={};});
    $('#clear').click(function(){$('#chars').val('');$('#cres').click()});
    $('#dimg').click(toggleImg);
    $('#clrcache').click(function(){sessionStorage.clear()});
    $('#genlink').click(shareLink);

    // Pre-Populate
    // Code based on:
    // https://stackoverflow.com/questions/14070105/pre-fill-form-field-via-url-in-html
    if(window.location.hash.substr(1).split('&') != "") {
        var given = window.location.hash.substr(1).split('&');
        for (var i = 0; i < given.length; i++) {
            var p = given[i].split('=');
            if(p.length < 2)
                continue;
            document.getElementById(p[0]).value =
                LZString.decompressFromEncodedURIComponent(p[1]);
        }

        $('#chars').submit();
    }

    // Hide the scrolly top guy
    $(window).scroll(function() {
        if($(window).scrollTop() > 50) {
            $('#gotop').fadeIn();
        } else {
            $('#gotop').fadeOut();
        }
    });

    console.log('Document Ready!');
});

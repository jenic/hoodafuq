# Hoodafuq

Eve Online utility using purely client-side code (HTML/CSS + JS) for "Local
Scan" character lookups.

![Example.png](sc/example.png "Example")

# Why?

Most utilities I've seen for Eve are server-side and/or closed-source tools.
This tool can be downloaded and ran locally without relying on someone else
paying server bills.

Also it was just something fun to do in my spare time.

# Requirements

Requires javascript (+ jQuery) with XHR requests to:

* CCP.is API
* zkillboard API

as well as
[SessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Storage)
support to cache results.

Tested in:

* Firefox
* Chrome (Chromium and Qutebrowser)

# Options

* Clear Results
    * Clear the characters displayed (will not clear cache)
* Clear Cache
    * Clear session storage cache of previously searched characters
* Clear All
    * Clear both results and cache
* Show/Hide Images
    * Slightly misleading. Toggles the actual fetching of images. Images that
      haven't been fetched already won't suddenly appear when toggled on. Not
      fetching images reduces lookup times and rendering greatly.
* Share Link
    * Being completely client-side, sharing scan results becomes problematic.
      This feature works decently enough for small samples but large fleet
      battles haven't been tested adequately. It should be noted that each user
      loading a shared link must refetch all results.

# Mirrors

You can download this and run it locally using file:///path/to/download but for
convenience I've also uploaded it to several servers for easy access:

* Gitlab CI: https://jenic.gitlab.io/hoodafuq/
* Goos.es: https://angry.goos.es/hoodafuq/
